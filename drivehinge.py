# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This scripted object represents a drive hinge.

The syntax is:

joint: <label>,
      drive hinge,
         <node 1>,
            hinge, euler, 0., 0., 0., # relative axis orientation
         <node 2>,
            hinge, euler, 0., 0., 0., # relative axis orientation
         single, 0., 0., 1., scalar function, "Fun_Arm_Input", # position
            multilinear,
               0.0, 0.,
               0.5, 0.,
               1.0, 0.;

label: an integer number to identify the joint, eg: 1,2,3...
node1: the label of the first structural node (this node will drive the second node)
node1: the label of the first structural node (this node will be driven)

'''

#from FreeCAD import Units
import FreeCAD
from sympy import Point3D, Line3D
import Draft

class Drivehinge:
    def __init__(self, obj, label, node1, node2):

        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)

        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z

        #x1 = node2.position_X
        #y1 = node2.position_Y
        #z1 = node2.position_Z

        obj.addExtension("App::GroupExtensionPython")

        #Create scripted object:
        obj.addProperty("App::PropertyString","label","drive hinge","label",1).label = label
        obj.addProperty("App::PropertyString","node 1","drive hinge","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","drive hinge","node 2",1).node_2 = node2.label
        obj.addProperty("App::PropertyString","joint","drive hinge","joint",1).joint = 'drive hinge'

        obj.addProperty("App::PropertyString","hinge orientation","drive hinge","hinge orientation").hinge_orientation = ""
        obj.addProperty("App::PropertyString","modifier","drive hinge","modifier").modifier = "*1.0"

        obj.addProperty("App::PropertyString","absolute position X","absolute position","absolute position X").absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","absolute position","absolute position Y").absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","absolute position","absolute position Z").absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"

        obj.addProperty("App::PropertyString","orientation matrix 1","orientation matrix","orientation matrix 1",1).orientation_matrix_1 = "3, 0.0, 0.0, 1.0, 2, guess"
        obj.addProperty("App::PropertyString","orientation matrix 2","orientation matrix","orientation matrix 2",1).orientation_matrix_2 = "3, 0.0, 0.0, 1.0, 2, guess"

        obj.addProperty("App::PropertyString","rotation axis","orientation","rotation axis",1).rotation_axis = '0., 0., 1.'

        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']

        obj.addProperty("App::PropertyString","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = '1'

        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '1'

        obj.Proxy = self

        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        length = FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: " + node1.label)[0].Length.Value # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)

        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(0, 0, 2*length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        #l.ViewObject.ArrowType = u"Dot"
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.DrawStyle = u"Dashed"

        #l.ViewObject.Selectable = False

        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        #d.ViewObject.Selectable = False
        d.Label = "jf: "+ label

        FreeCAD.ActiveDocument.recompute()

    def execute(self, fp):
            ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line
            precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
            #Two 3D points that define the joint´s line:
            p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2])
            #A line that defines the joint´s orientation:
            l1 = Line3D(p1, p2)#Line that defines the joint
            #generate the orientation matrix:
            fp.orientation_matrix_1 = "3,"+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"
            fp.orientation_matrix_2 = "3,"+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"
            #fp.rotation_axis = str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission))

            FreeCAD.Console.PrintMessage("DRIVE HINGE JOINT: " +fp.label+" successful recomputation...\n")

