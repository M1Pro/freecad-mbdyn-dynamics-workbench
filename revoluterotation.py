# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
8.12.39 Revolute rotation
This joint allows the relative rotation of two nodes about a given axis, which is axis 3 in the reference
systems defined by the two orientation statements. The relative position is not constrained.



     joint: 2, #hinge label
            revolute rotation,
            1, #node 1
                position, 0.0, 0.0, 0.0, #offset relative to node 1 [m]
                orientation, 3, -299.95432920955600000000, -5.23572118844330000000, 0, 2, guess, #orientation matrix relative to node 1
            2, #node 2
                position, 0.0, 0.0, -0.10, #Offset relative to node 2 [m]
                orientation, 3, -299.95432920955600000000, -5.23572118844330000000, 0, 2, guess; #orientation matrix relative to node 2

Rationale
A revolute joint without position constraints; this joint, in conjunction with an inline joint, should be
used to constrain, for example, the two nodes of a hydraulic actuator.
'''

#from FreeCAD import Units
import FreeCAD
from sympy import Point3D, Line3D
import Draft

class Revoluterotation:
    def __init__(self, obj, label, node1, node2, reference, reference1):

        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)

        #Get the reference's center of mass, to define the orientation line:

        try:
            x4 = FreeCAD.Units.Quantity(reference.Curve.Center[0],FreeCAD.Units.Unit('mm'))
            y4 = FreeCAD.Units.Quantity(reference.Curve.Center[1],FreeCAD.Units.Unit('mm'))
            z4 = FreeCAD.Units.Quantity(reference.Curve.Center[2],FreeCAD.Units.Unit('mm'))
        except:
            x4 = FreeCAD.Units.Quantity(reference.CenterOfMass[0],FreeCAD.Units.Unit('mm'))
            y4 = FreeCAD.Units.Quantity(reference.CenterOfMass[1],FreeCAD.Units.Unit('mm'))
            z4 = FreeCAD.Units.Quantity(reference.CenterOfMass[2],FreeCAD.Units.Unit('mm'))

        try:
            x3 = FreeCAD.Units.Quantity(reference1.Curve.Center[0],FreeCAD.Units.Unit('mm'))
            y3 = FreeCAD.Units.Quantity(reference1.Curve.Center[1],FreeCAD.Units.Unit('mm'))
            z3 = FreeCAD.Units.Quantity(reference1.Curve.Center[2],FreeCAD.Units.Unit('mm'))
        except:
            x3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[0],FreeCAD.Units.Unit('mm'))
            y3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[1],FreeCAD.Units.Unit('mm'))
            z3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[2],FreeCAD.Units.Unit('mm'))

        #nodes´s position:
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z

        x1 = 0.
        y1 = 0.
        z1 = 0.

        x2 = 0.
        y2 = 0.
        z2 = 0.

        #Calculate the relative offset 1:
        #x1 = x-node1.position_X
        #y1 = y-node1.position_Y
        #z1 = z-node1.position_Z

        #Calculate the relative offset 2:
        #x2 = x-node2.position_X
        #y2 = y-node2.position_Y
        #z2 = z-node2.position_Z

        obj.addExtension("App::GroupExtensionPython")

        #Create scripted object:
        obj.addProperty("App::PropertyString","label","revolute rotation","label",1).label = label
        obj.addProperty("App::PropertyString","node 1","revolute rotation","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","revolute rotation","node 2",1).node_2 = node2.label
        obj.addProperty("App::PropertyString","joint","revolute rotation","joint",1).joint = 'revolute rotation'

        #Absolute pin position:
        obj.addProperty("App::PropertyString","absolute position X","absolute position","absolute position X").absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","absolute position","absolute position Y").absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","absolute position","absolute position Z").absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"

        #Relative offset 1:
        obj.addProperty("App::PropertyString","relative offset 1 X","relative offset 1","relative offset 1 X").relative_offset_1_X = str(round(x1,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Y","relative offset 1","relative offset 1 Y").relative_offset_1_Y = str(round(y1,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Z","relative offset 1","relative offset 1 Z").relative_offset_1_Z = str(round(z1,precission))+" m"

        #Relative offset 2:
        obj.addProperty("App::PropertyString","relative offset 2 X","relative offset 2","relative offset 2 X").relative_offset_2_X = str(round(x2,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 2 Y","relative offset 2","relative offset 2 Y").relative_offset_2_Y = str(round(y2,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 2 Z","relative offset 2","relative offset 2 Z").relative_offset_2_Z = str(round(z2,precission))+" m"

        obj.addProperty("App::PropertyString","orientation matrix 1","revolute rotation","orientation matrix 1").orientation_matrix_1 = "3, 0.0, 0.0, 1.0, 2, guess"
        obj.addProperty("App::PropertyString","orientation matrix 2","revolute rotation","orientation matrix 2").orientation_matrix_2 = "3, 0.0, 0.0, 1.0, 2, guess"

        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']

        obj.addProperty("App::PropertyString","force vector multiplier","animation","force vector multiplier").force_vector_multiplier = '1'

        obj.addProperty("App::PropertyString","structural dummy","animation","structural dummy").structural_dummy = '1'

        obj.Proxy = self

 #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:

        p1 = FreeCAD.Vector(x4, y4, z4)
        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(x3, y3, z3)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.ViewObject.DrawStyle = u"Dashed"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00

        #Add the vector to visualize reaction forces
        p1 = FreeCAD.Vector(0, 0, 0)
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p2 = FreeCAD.Vector(Llength, Llength, Llength)
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)
        d.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label

    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places

        ##############################################################################Calculate the new orientation:
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2])
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:
        fp.orientation_matrix_1 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"
        fp.orientation_matrix_2 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"

        ##############################################################################Recalculate the offset, in case any of the two nodes was moved:
        #get the pin position (the pin position is not expected to change):
        x = float(fp.absolute_position_X.split(" ")[0])
        y = float(fp.absolute_position_Y.split(" ")[0])
        z = float(fp.absolute_position_Z.split(" ")[0])

        #get the node´s position:
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]

        #Re-calculate the joint possition relative to it's nodes (relative offset)
        x1 = x - node1.absolute_position_X.getValueAs("m").Value
        y1 = y - node1.absolute_position_Y.getValueAs("m").Value
        z1 = z - node1.absolute_position_Z.getValueAs("m").Value

        x2 = x - node2.absolute_position_X.getValueAs("m").Value
        y2 = y - node2.absolute_position_Y.getValueAs("m").Value
        z2 = z - node2.absolute_position_Z.getValueAs("m").Value

        #Update the offset:
        #fp.relative_offset_1_X = str(round(x1,precission))+" m"
        #fp.relative_offset_1_Y = str(round(y1,precission))+" m"
        #fp.relative_offset_1_Z = str(round(z1,precission))+" m"

        #fp.relative_offset_2_X = str(round(x2,precission))+" m"
        #fp.relative_offset_2_Y = str(round(y2,precission))+" m"
        #fp.relative_offset_2_Z = str(round(z2,precission))+" m"

        FreeCAD.Console.PrintMessage("REVOLUTE ROTATION JOINT: " +fp.label+" successful recomputation...\n")
