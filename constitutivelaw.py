# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################


import FreeCAD

class ConstitutiveLaw:
    def __init__(self, obj, label):

        obj.addExtension("App::GroupExtensionPython")

        obj.addProperty("App::PropertyString","label","constitutive law","label",1).label = label

        #law type
        obj.addProperty("App::PropertyEnumeration","type","constitutive law","type")
        obj.type=['linear elastic isotropic, <stiffness>',
                  'linear elastic generic, <stiffness>',
                  'linear viscous isotropic, <viscosity>',
                  'linear viscous generic, <viscosity>',
                  'linear viscoelastic isotropic, <stiffness>, <viscosity>',
                  'linear viscoelastic generic, <stiffness>, <viscosity>']

        #law dimensionality
        obj.addProperty("App::PropertyEnumeration","dimensionality","constitutive law","dimensionality")
        obj.dimensionality=['1D','3D','6D']

        #1D scalar coefficients:
        obj.addProperty("App::PropertyString","stiffness","1D scalar stiffness","stiffness").stiffness = '1.e9'
        obj.addProperty("App::PropertyString","viscosity","1D scalar viscosity","viscosity").viscosity = '1.e9'

        #3D stiffness matrix:
        obj.addProperty("App::PropertyString","_3Dsa11","3D stiffness matrix","_3Dsa11")._3Dsa11 = '1.e9'
        obj.addProperty("App::PropertyString","_3Dsa12","3D stiffness matrix","_3Dsa12")._3Dsa12 = '0.'
        obj.addProperty("App::PropertyString","_3Dsa13","3D stiffness matrix","_3Dsa13")._3Dsa13 = '0.'

        obj.addProperty("App::PropertyString","_3Dsa21","3D stiffness matrix","_3Dsa21")._3Dsa21 = '.0'
        obj.addProperty("App::PropertyString","_3Dsa22","3D stiffness matrix","_3Dsa22")._3Dsa22 = '1.e6'
        obj.addProperty("App::PropertyString","_3Dsa23","3D stiffness matrix","_3Dsa23")._3Dsa23 = '-1.e5'

        obj.addProperty("App::PropertyString","_3Dsa31","3D stiffness matrix","_3Dsa31")._3Dsa31 = '0.'
        obj.addProperty("App::PropertyString","_3Dsa32","3D stiffness matrix","_3Dsa32")._3Dsa32 = '0.'
        obj.addProperty("App::PropertyString","_3Dsa33","3D stiffness matrix","_3Dsa33")._3Dsa33 = '1.e6'

        #3D viscocity matrix:
        obj.addProperty("App::PropertyString","_3Dva11","3D viscosity matrix","_3Dva11")._3Dva11 = '1.e9'
        obj.addProperty("App::PropertyString","_3Dva12","3D viscosity matrix","_3Dva12")._3Dva12 = '0.'
        obj.addProperty("App::PropertyString","_3Dva13","3D viscosity matrix","_3Dva13")._3Dva13 = '0.'

        obj.addProperty("App::PropertyString","_3Dva21","3D viscosity matrix","_3Dva21")._3Dva21 = '.0'
        obj.addProperty("App::PropertyString","_3Dva22","3D viscosity matrix","_3Dva22")._3Dva22 = '1.e6'
        obj.addProperty("App::PropertyString","_3Dva23","3D viscosity matrix","_3Dva23")._3Dva23 = '-1.e5'

        obj.addProperty("App::PropertyString","_3Dva31","3D viscosity matrix","_3Dva31")._3Dva31 = '0.'
        obj.addProperty("App::PropertyString","_3Dva32","3D viscosity matrix","_3Dva32")._3Dva32 = '0.'
        obj.addProperty("App::PropertyString","_3Dva33","3D viscosity matrix","_3Dva33")._3Dva33 = '1.e6'

        #6D stiffness matrix:
        obj.addProperty("App::PropertyString","_6Dsa11","6D stiffness matrix","_6Dsa11")._6Dsa11 = '1.5e6'
        obj.addProperty("App::PropertyString","_6Dsa12","6D stiffness matrix","_6Dsa12")._6Dsa12 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa13","6D stiffness matrix","_6Dsa13")._6Dsa13 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa14","6D stiffness matrix","_6Dsa14")._6Dsa14 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa15","6D stiffness matrix","_6Dsa15")._6Dsa15 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa16","6D stiffness matrix","_6Dsa16")._6Dsa16 = '0.'

        obj.addProperty("App::PropertyString","_6Dsa21","6D stiffness matrix","_6Dsa21")._6Dsa21 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa22","6D stiffness matrix","_6Dsa22")._6Dsa22 = '0.6e6'
        obj.addProperty("App::PropertyString","_6Dsa23","6D stiffness matrix","_6Dsa23")._6Dsa23 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa24","6D stiffness matrix","_6Dsa24")._6Dsa24 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa25","6D stiffness matrix","_6Dsa25")._6Dsa25 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa26","6D stiffness matrix","_6Dsa26")._6Dsa26 = '0.'

        obj.addProperty("App::PropertyString","_6Dsa31","6D stiffness matrix","_6Dsa31")._6Dsa31 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa32","6D stiffness matrix","_6Dsa32")._6Dsa32 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa33","6D stiffness matrix","_6Dsa33")._6Dsa33 = '0.6e6'
        obj.addProperty("App::PropertyString","_6Dsa34","6D stiffness matrix","_6Dsa34")._6Dsa34 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa35","6D stiffness matrix","_6Dsa35")._6Dsa35 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa36","6D stiffness matrix","_6Dsa36")._6Dsa36 = '0.'

        obj.addProperty("App::PropertyString","_6Dsa41","6D stiffness matrix","_6Dsa41")._6Dsa41 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa42","6D stiffness matrix","_6Dsa42")._6Dsa42 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa43","6D stiffness matrix","_6Dsa43")._6Dsa43 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa44","6D stiffness matrix","_6Dsa44")._6Dsa44 = '0.4e3'
        obj.addProperty("App::PropertyString","_6Dsa45","6D stiffness matrix","_6Dsa45")._6Dsa45 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa46","6D stiffness matrix","_6Dsa46")._6Dsa46 = '0.'

        obj.addProperty("App::PropertyString","_6Dsa51","6D stiffness matrix","_6Dsa51")._6Dsa51 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa52","6D stiffness matrix","_6Dsa52")._6Dsa52 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa53","6D stiffness matrix","_6Dsa53")._6Dsa53 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa54","6D stiffness matrix","_6Dsa54")._6Dsa54 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa55","6D stiffness matrix","_6Dsa55")._6Dsa55 = '1.e3'
        obj.addProperty("App::PropertyString","_6Dsa56","6D stiffness matrix","_6Dsa56")._6Dsa56 = '0.'

        obj.addProperty("App::PropertyString","_6Dsa61","6D stiffness matrix","_6Dsa61")._6Dsa61 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa62","6D stiffness matrix","_6Dsa62")._6Dsa62 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa63","6D stiffness matrix","_6Dsa63")._6Dsa63 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa64","6D stiffness matrix","_6Dsa64")._6Dsa64 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa65","6D stiffness matrix","_6Dsa65")._6Dsa65 = '0.'
        obj.addProperty("App::PropertyString","_6Dsa66","6D stiffness matrix","_6Dsa66")._6Dsa66 = '1.e3'

        #6D viscocity matrix:
        obj.addProperty("App::PropertyString","_6Dva11","6D viscosity matrix","_6Dva11")._6Dva11 = '0.'
        obj.addProperty("App::PropertyString","_6Dva12","6D viscosity matrix","_6Dva12")._6Dva12 = '0.'
        obj.addProperty("App::PropertyString","_6Dva13","6D viscosity matrix","_6Dva13")._6Dva13 = '0.'
        obj.addProperty("App::PropertyString","_6Dva14","6D viscosity matrix","_6Dva14")._6Dva14 = '0.'
        obj.addProperty("App::PropertyString","_6Dva15","6D viscosity matrix","_6Dva15")._6Dva15 = '0.'
        obj.addProperty("App::PropertyString","_6Dva16","6D viscosity matrix","_6Dva16")._6Dva16 = '0.'

        obj.addProperty("App::PropertyString","_6Dva21","6D viscosity matrix","_6Dva21")._6Dva21 = '0.'
        obj.addProperty("App::PropertyString","_6Dva22","6D viscosity matrix","_6Dva22")._6Dva22 = '0.'
        obj.addProperty("App::PropertyString","_6Dva23","6D viscosity matrix","_6Dva23")._6Dva23 = '0.'
        obj.addProperty("App::PropertyString","_6Dva24","6D viscosity matrix","_6Dva24")._6Dva24 = '0.'
        obj.addProperty("App::PropertyString","_6Dva25","6D viscosity matrix","_6Dva25")._6Dva25 = '0.'
        obj.addProperty("App::PropertyString","_6Dva26","6D viscosity matrix","_6Dva26")._6Dva26 = '0.'

        obj.addProperty("App::PropertyString","_6Dva31","6D viscosity matrix","_6Dva31")._6Dva31 = '0.'
        obj.addProperty("App::PropertyString","_6Dva32","6D viscosity matrix","_6Dva32")._6Dva32 = '0.'
        obj.addProperty("App::PropertyString","_6Dva33","6D viscosity matrix","_6Dva33")._6Dva33 = '0.'
        obj.addProperty("App::PropertyString","_6Dva34","6D viscosity matrix","_6Dva34")._6Dva34 = '0.'
        obj.addProperty("App::PropertyString","_6Dva35","6D viscosity matrix","_6Dva35")._6Dva35 = '0.'
        obj.addProperty("App::PropertyString","_6Dva36","6D viscosity matrix","_6Dva36")._6Dva36 = '0.'

        obj.addProperty("App::PropertyString","_6Dva41","6D viscosity matrix","_6Dva41")._6Dva41 = '0.'
        obj.addProperty("App::PropertyString","_6Dva42","6D viscosity matrix","_6Dva42")._6Dva42 = '0.'
        obj.addProperty("App::PropertyString","_6Dva43","6D viscosity matrix","_6Dva43")._6Dva43 = '0.'
        obj.addProperty("App::PropertyString","_6Dva44","6D viscosity matrix","_6Dva44")._6Dva44 = '0.'
        obj.addProperty("App::PropertyString","_6Dva45","6D viscosity matrix","_6Dva45")._6Dva45 = '0.'
        obj.addProperty("App::PropertyString","_6Dva46","6D viscosity matrix","_6Dva46")._6Dva46 = '0.'

        obj.addProperty("App::PropertyString","_6Dva51","6D viscosity matrix","_6Dva51")._6Dva51 = '0.'
        obj.addProperty("App::PropertyString","_6Dva52","6D viscosity matrix","_6Dva52")._6Dva52 = '0.'
        obj.addProperty("App::PropertyString","_6Dva53","6D viscosity matrix","_6Dva53")._6Dva53 = '0.'
        obj.addProperty("App::PropertyString","_6Dva54","6D viscosity matrix","_6Dva54")._6Dva54 = '0.'
        obj.addProperty("App::PropertyString","_6Dva55","6D viscosity matrix","_6Dva55")._6Dva55 = '0.'
        obj.addProperty("App::PropertyString","_6Dva56","6D viscosity matrix","_6Dva56")._6Dva56 = '0.'

        obj.addProperty("App::PropertyString","_6Dva61","6D viscosity matrix","_6Dva61")._6Dva61 = '0.'
        obj.addProperty("App::PropertyString","_6Dva62","6D viscosity matrix","_6Dva62")._6Dva62 = '0.'
        obj.addProperty("App::PropertyString","_6Dva63","6D viscosity matrix","_6Dva63")._6Dva63 = '0.'
        obj.addProperty("App::PropertyString","_6Dva64","6D viscosity matrix","_6Dva64")._6Dva64 = '0.'
        obj.addProperty("App::PropertyString","_6Dva65","6D viscosity matrix","_6Dva65")._6Dva65 = '0.'
        obj.addProperty("App::PropertyString","_6Dva66","6D viscosity matrix","_6Dva66")._6Dva66 = '0.'

        obj.addProperty("App::PropertyString","expression","constitutive law","expression",1).expression = ''


        obj.Proxy = self

        FreeCAD.ActiveDocument.recompute()

    def execute(self, fp):

        if(fp.type == 'linear elastic isotropic, <stiffness>'):
            fp.expression = 'linear elastic isotropic, ' + fp.stiffness

        if(fp.type == 'linear elastic generic, <stiffness>'):
            if(fp.dimensionality == '1D'):
                fp.expression = 'linear elastic generic, ' + fp.stiffness

            if(fp.dimensionality == '3D'):
                fp.expression = 'linear elastic generic, ' + fp._3Dsa11 +', ' + fp._3Dsa12 +', ' + fp._3Dsa13 +',\n' + '                                      ' + fp._3Dsa21 +', ' + fp._3Dsa22 +', ' + fp._3Dsa23 +', \n' + '                                      ' + fp._3Dsa31 +', ' + fp._3Dsa32 +', ' + fp._3Dsa33

            if(fp.dimensionality == '6D'):
                fp.expression = 'linear elastic generic, ' + fp._6Dsa11 +', ' + fp._6Dsa12 +', ' + fp._6Dsa13 +', ' + fp._6Dsa14 +', ' + fp._6Dsa15 +', ' + fp._6Dsa16 +',\n' + '                                      ' + fp._6Dsa21 +', ' + fp._6Dsa22 +', ' + fp._6Dsa23 +', ' + fp._6Dsa24 +', ' + fp._6Dsa25 +', ' + fp._6Dsa26 +',\n' + '                                      ' + fp._6Dsa31 +', ' + fp._6Dsa32 +', ' + fp._6Dsa33 +', ' + fp._6Dsa34 +', ' + fp._6Dsa35 +', ' + fp._6Dsa36 +',\n' + '                                      ' + fp._6Dsa41 +', ' + fp._6Dsa42 +', ' + fp._6Dsa43 +', ' + fp._6Dsa44 +', ' + fp._6Dsa45 +', ' + fp._6Dsa46 +',\n' + '                                      ' + fp._6Dsa51 +', ' + fp._6Dsa52 +', ' + fp._6Dsa53 +', ' + fp._6Dsa54 +', ' + fp._6Dsa55 +', ' + fp._6Dsa56 +',\n' + '                                      ' + fp._6Dsa61 +', ' + fp._6Dsa62 +', ' + fp._6Dsa63 +', ' + fp._6Dsa64 +', ' + fp._6Dsa65 +', ' + fp._6Dsa66

        if(fp.type == 'linear viscoelastic isotropic, <stiffness>, <viscosity>'):
            fp.expression = 'linear viscoelastic isotropic, ' + fp.stiffness + ', ' + fp.viscosity


        if(fp.type == 'linear viscous isotropic, <viscosity>'):
            fp.expression = 'linear viscous isotropic, ' + fp.viscosity

        """

        if(fp.type == 'linear viscous generic, <viscosity>'):
            fp.expression = 'linear viscous generic, ' + fp.viscosity



        if(fp.type == 'linear viscoelastic generic, <stiffness>, <viscosity>'):
            fp.expression = 'linear viscoelastic generic, ' + fp.stiffness + ', ' + fp.viscosity
        """
        pass
