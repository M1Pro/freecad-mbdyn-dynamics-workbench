# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This class receives a non-parametric body and creates a structural static node positioned at the body's center of mass.
A structural static node is an owner of kinematic degrees of freedom.
It can assume the six degrees of freedom, three for possition and three for orientation.

structural: <label>,
            static,
            <position>,
            <orientation>,
            <velocity>,
            <angular velocity> ;
Where:
<label> is an integer number that identifies the node. Label the 3D body in FreeCAD with a unique integer number to identify it's node, before you call this class.
<position> is the node's possition relative to the absolute reference frame. This is equal to the body's center of mass position.
<orientation> is the node's orientation relative to the absolute reference frame. I assume all bodies start at: euler, 0., 0., 0.
<velocity> is the initial nodes's velocity. The user can define this from FreeCAD
<angular velocity> the initial angular velocity. To be defined by the user too.

Note that static nodes are not supposed to have initial velocity or angular velocity?. In general, a clamp joint will be used to "clamp" the static node to the gobal inertial frame.
The static node can then be used to apply, for instance, an axial rotation joint to rotate a dynamic node.
'''

import FreeCAD
#import FreeCADGui
import Draft

#from FreeCAD.Units.Units import FreeCAD.Units.Unit,FreeCAD.Units.Quantity

class Structuralstaticnode:
    def __init__(self, obj, baseBody):


        try:
            x = FreeCAD.Units.Quantity(baseBody.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
            y = FreeCAD.Units.Quantity(baseBody.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
            z = FreeCAD.Units.Quantity(baseBody.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
            length = (baseBody.Shape.BoundBox.XLength+baseBody.Shape.BoundBox.YLength+baseBody.Shape.BoundBox.ZLength)/6 # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        except:#If a point, instead of an object, was selected as reference:
            x = FreeCAD.Units.Quantity(baseBody.X,FreeCAD.Units.Unit('mm'))
            y = FreeCAD.Units.Quantity(baseBody.Y,FreeCAD.Units.Unit('mm'))
            z = FreeCAD.Units.Quantity(baseBody.Z,FreeCAD.Units.Unit('mm'))
            length = FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/10


        #Initial absolute orienation (Euler angles):
        yaw = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))
        pitch = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))
        roll = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))

        #Initial velocity (mm/sec):
        vx = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm/s'))
        vy = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm/s'))
        vz = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm/s'))

        #Initial angular velocity (deg/sec):
        wx = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg/s'))
        wy = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg/s'))
        wz = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg/s'))

        #Give the object the ability to contain other objetcs:
        obj.addExtension("App::GroupExtensionPython")

        #Give the object its properties:
        obj.addProperty("App::PropertyString","label","structural static node","label",1).label = baseBody.Label
        obj.addProperty("App::PropertyString","type","structural static node","type",1).type = 'static'
        obj.addProperty("App::PropertyString","plugin variables","structural static node","plugin variables",1).plugin_variables = "none"

        obj.addProperty("App::PropertyDistance","absolute position X","absolute position","absolute position X").absolute_position_X = x
        obj.addProperty("App::PropertyDistance","absolute position Y","absolute position","absolute position Y").absolute_position_Y = y
        obj.addProperty("App::PropertyDistance","absolute position Z","absolute position","absolute position Z").absolute_position_Z = z

        obj.addProperty("App::PropertyAngle","yaw","absolute orientation matrix","yaw").yaw = yaw
        obj.addProperty("App::PropertyAngle","pitch","absolute orientation matrix","pitch").pitch = pitch
        obj.addProperty("App::PropertyAngle","roll","absolute orientation matrix","roll").roll = roll

        obj.addProperty("App::PropertySpeed","absolute velocity X","absolute velocity","absolute velocity_X").absolute_velocity_X = vx
        obj.addProperty("App::PropertySpeed","absolute velocity Y","absolute velocity","absolute velocity_Y").absolute_velocity_Y = vy
        obj.addProperty("App::PropertySpeed","absolute velocity Z","absolute velocity","absolute velocity_Z").absolute_velocity_Z = vz

        obj.addProperty("App::PropertyString","absolute angular velocity X","absolute angular velocity","absolute angular velocity X").absolute_angular_velocity_X = str(wx)
        obj.addProperty("App::PropertyString","absolute angular velocity Y","absolute angular velocity","absolute angular velocity Y").absolute_angular_velocity_Y = str(wy)
        obj.addProperty("App::PropertyString","absolute angular velocity Z","absolute angular velocity","absolute angular velocity Z").absolute_angular_velocity_Z = str(wz)

        obj.Proxy = self

        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:

        p1 = FreeCAD.Vector(0, 0, 0)
        #Add x vector of the coordinate system:
        p2 = FreeCAD.Vector(length, 0, 0)
        l = Draft.makeLine(p1, p2)
        l.Label = 'x: structural: '+ baseBody.Label
        l.ViewObject.LineColor = (1.00,0.00,0.00)
        l.ViewObject.PointColor = (1.00,0.00,0.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'
        #Add y vector of the coordinate system:
        p2 = FreeCAD.Vector(0, length, 0)
        l = Draft.makeLine(p1, p2)
        l.Label = 'y: structural: '+ baseBody.Label
        l.ViewObject.LineColor = (0.00,1.00,0.00)
        l.ViewObject.PointColor = (0.00,1.00,0.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'
        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(0, 0, 0+length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: structural: '+ baseBody.Label
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'


    def onChanged(self, fp, prop):
        '''Do something when a property has changed'''
        try:
            if str(prop)=="yaw" or str(prop)=="pitch" or str(prop)=="roll" or str(prop)=="absolute_position_X" or str(prop)=="absolute_position_Y" or str(prop)=="absolute_position_Z":
                #Get the new position and orientation
                x = fp.absolute_position_X
                y = fp.absolute_position_Y
                z = fp.absolute_position_Z
                yaw = fp.yaw
                pitch = fp.pitch
                roll = fp.roll
                #Move the arrows and text
                FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(yaw,pitch,roll), FreeCAD.Vector(0,0,0))
                FreeCAD.ActiveDocument.getObjectsByLabel("y: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(yaw,pitch,roll), FreeCAD.Vector(0,0,0))
                FreeCAD.ActiveDocument.getObjectsByLabel("z: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(yaw,pitch,roll), FreeCAD.Vector(0,0,0))
        except:
            pass

    def execute(self, fp):
        FreeCAD.Console.PrintMessage("STATIC NODE: " +fp.label+" successful recomputation...\n")

